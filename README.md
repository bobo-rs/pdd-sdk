# 拼多多商品管理接口PHP-SDK

## 忽略文件目录
```
*.lock
.idea/
.git/
/vendor

```
## 文件目录
```
pdd-sdk───────────────────────────────────────────应用目录
│   .gitignore────────────────────────────────────忽略文件目录
│   autoload.php──────────────────────────────────自动加载文件
│   composer.json─────────────────────────────────composer 引入包
│   LICENSE───────────────────────────────────────版权
│   README.md─────────────────────────────────────readme文件
├───example───────────────────────────────────────示例目录
│       example.php───────────────────────────────示例文件
│       example_file.php──────────────────────────图片上传示例文件
│       loader.php────────────────────────────────配置加载
├───src───────────────────────────────────────────运行时文件
│   └───PDDCore───────────────────────────────────核心代码文件
│       │   ArrayList.php─────────────────────────数据格式类
│       │   RequestCheckUtil.php──────────────────基础验证类
│       │   ResultSet.php─────────────────────────结果返回类
│       │   TopClient.php─────────────────────────客户端请求类
│       │   TopLogger.php─────────────────────────日志
│       │
│       ├───Contract──────────────────────────────接口类目录
│       │       GoodsInterface.php────────────────商品接口定义类
│       │
│       ├───Request───────────────────────────────封装接口目录
│       │       PddGoodsAddRequest.php
│       │       PddGoodsAdvicePriceGetRequest.php
│       │       PddGoodsAuthorizationCatsRequest.php
│       │       PddGoodsCatRuleGetRequest.php
│       │       PddGoodsCatsGetRequest.php
│       │       PddGoodsCommitDetailGetRequest.php
│       │       PddGoodsCommitListGetRequest.php
│       │       PddGoodsCommitStatusGetRequest.php
│       │       PddGoodsCountryGet.php
│       │       PddGoodsDetailGetRequest.php
│       │       PddGoodsEditGoodsCommitRequest.php
│       │       PddGoodsFilespaceImageUploadRequest.php
│       │       PddGoodsImageUploadRequest.php
│       │       PddGoodsInformationGetRequest.php
│       │       PddGoodsInformationUpdateRequest.php
│       │       PddGoodsLatestCommitStatusGetRequest.php
│       │       PddGoodsListGetRequest.php
│       │       PddGoodsLogisticsTemplateGetRequest.php
│       │       PddGoodsMaterialCreateRequest.php
│       │       PddGoodsMaterialDeleteRequest.php
│       │       PddGoodsMaterialQueryRequest.php
│       │       PddGoodsOutPropertyMappingGetRequest.php
│       │       PddGoodsQuantityUpdateRequest.php
│       │       PddGoodsSkuMeasurementListRequest.php
│       │       PddGoodsSkuPriceUpdateRequest.php
│       │       PddGoodsSpecGetRequest.php
│       │       PddGoodsSpecIdGetRequest.php
│       │       PddGoodsSpuGetRequest.php
│       │       PddGoodsSpuSearchRequest.php
│       │       PddGoodsSubmitGoodsCommitRequest.php
│       │       PddGoodsTemplatePropertyValueSearchRequest.php
│       │       PddGoodsVideoUploadRequest.php
│       │       PddOneExpressCostTemplateRequest.php
│       │       PddPopAuthTokenCreateRequest.php
│       │       PddPopAuthTokenRefreshRequest.php
│       │       ..................................
│       └───Util──────────────────────────────────基础扩展属性类目
│               CarouselVideoItemUtil.php
│               ElecGoodsAttributesUtil.php
│               GoodsPropertiesItemUtil.php
│               GoodsTradeAttrBookingNotesUtil.php
│               GoodsTradeAttrUtil.php
│               GoodsTravelAttrUtil.php
│               KeyPropItemUtil.php
│               OverseaGoodsUtil.php
│               RequestUtil.php
│               SkuListItemUtil.php
│               SkuListOverseaSkuUtil.php
│               SkuListSkuPropertiesItemUtil.php
│               SkuPriceListItemUtil.php
│               ..............................
```
## 使用方法

```php
// 上传文件示例

require_once "loader.php";

use PDDCore\Request;

$gatewayUrl = "https://gw-upload.pinduoduo.com/api/upload";

$fileUrl = $_POST['file'];
if (!$fileUrl){
    exit(json_encode([
        "code"=>1001,
        "msg"=>"请上传文件~"
    ],JSON_UNESCAPED_UNICODE));
}

//$fileUrl = "https://t00img.yangkeduo.com/garner-api-open/59ad8dfcbc424745b1a88aa0ffae8a5e.jpg";
$client = new PDDCore\TopClient(APPKEY,SECRET);
$client->setGatewayUrl($gatewayUrl);
// 上传文件
$requset = new Request\PddGoodsFilespaceImageUploadRequest();
$requset->setFile($fileUrl); // 注：此字段不参与签名

// 执行请求
$res = $client->execute($requset,ACCESS_TOKEN,'file');
exit(json_encode($res,JSON_UNESCAPED_UNICODE));


// 商品列表或详情请求示例

require_once "loader.php";

use PDDCore\Request;

$data = [];
if ($_GET){
    $data = $_GET;
}elseif($_POST){
    $data = $_POST;
}
if (!$data){
    exit(json_encode([
        "code"=>1001,
        "msg"=>"参数不能为空~",
        "data"=>[]
    ],JSON_UNESCAPED_UNICODE));
}
try{
    $check = \PDDCore\RequestCheckUtil::checkInValue(1,[1,3],"fffff");
}catch (\Exception $e){
    exit(json_encode([
        "code"=>$e->getCode(),
        "error_msg"=>$e->getMessage(),
        "desc"=>$e->getTraceAsString()
    ],JSON_UNESCAPED_UNICODE));
}

$client = new PDDCore\TopClient(APPKEY,SECRET);
$client->setGatewayUrl(GATEWAY_URL);

// 商品列表
$req = new Request\PddGoodsListGetRequest();
$req->setPageSize($data['pageSize']);
$req->setPage($data['page']);

// 商品详情
//$req = new Request\PddGoodsInformationGetRequest();
//$req->setGoodsId($data['goodsId']);

$res = $client->execute($req,ACCESS_TOKEN);
exit(json_encode(
    $res,JSON_UNESCAPED_UNICODE
));
```
```
调试步骤示例
    1.打开调试目录：example
    2.loader.php        基础请求自动加载配置文件，配置在拼多多申请appkey、secret、accessToken、gatewayUrl
    3.example.php       基础请求文件，直接访问可以查询商品列表和详情，相应参数
        3.01 page       页码
        3.02 pageSize   页码条数
        3.03 goodsId    商品ID【拼多多】
        3.04 注意事项：page和pageSize请求商品列表必传，goodsId请求商品详情必传；
    4.example_file.php  文件上传示例，调用此文件可直接上传图片文件到拼多多图片空间
        4.01 file       文件地址，注：可外部远程地址本地地址
        4.02 注意事项：file字段不参与签名，`@`符号自动拼接在前面，示例：@http://xxxxxxxxx.jpg
    
```
## 应用版本
1. PHP版本支持7.2以上版本（含7.2），也支持8.0以上版本，低版本不支持
2. 拼多多接口版本：v1

## 开发规范
~~~
1.命名规范
    SDK遵循PSR-2命名规范和PSR-4自动加载规范，并且注意如下规范：

2.目录和文件
    核心运行时目录使用大写开头+下划线；
    类库、函数文件统一以.php为后缀；
    类的文件名均以命名空间定义，并且命名空间的路径和类库文件所在路径一致；
    类（包含接口和Trait）文件采用驼峰法命名（首字母大写），其它文件采用小写+下划线命名；
    类名（包括接口和Trait）和文件名保持一致，统一采用驼峰法命名（首字母大写）；

3.函数和类、属性命名
    类的命名采用驼峰法（首字母大写），例如 User、UserType；
    函数的命名使用小写字母和下划线（小写字母开头）的方式，例如 get_client_ip；
    方法的命名使用驼峰法（首字母小写），例如 getUserName；
    属性的命名使用驼峰法（首字母小写），例如 tableName、instance；
    特例：以双下划线__打头的函数或方法作为魔术方法，例如 __call 和 __autoload；

4.常量和配置
    常量以大写字母和下划线命名，例如 APP_PATH；
    配置参数以小写字母和下划线命名，例如 url_route_on 和url_convert；
    环境变量定义使用大写字母和下划线命名，例如APP_DEBUG；
~~~

## 注意事项
1. SDK经过成熟项目允许，请放心使用
2. SDK目前只支持拼多多商品添加、编辑、列表获取、上下架等接口，后续会继续扩展，也可以在基础上自行开发；
3. vendor文件不提交到远程仓库，获取代码没有vendor可以执行composer dump-autoload
4. 若更新下来的包不能加入自己项目时，请切换到SDK目录，例：`xxx/pdd-sdk/pdd-sdk`目录，执行查看是否存在`.git`目录，存在则删除掉，就可以加入同项目版本库；
5. composer取包命令
```
 默认取包命令：composer require pdd-sdk/pdd-sdk
 若是失败可用： composer require pdd-sdk/pdd-sdk dev-master
 更新包命令：composer update pdd-sdk/pdd-sdk
```

## 完善代码
1. 第一版暂时支持：商户商品管理模块，后续会继续增加用户信息、订单管理等接口
2. 持续开发中......
