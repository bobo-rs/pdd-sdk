<?php

/**
 * 自动加载class类
 * @param $class
 */
function autoloader($class){
    $path = str_replace('\\',DIRECTORY_SEPARATOR,$class);
    $classFile = __DIR__."/src/".$path.".php";
    if (file_exists($classFile)){
        require_once $classFile;
    }
}
spl_autoload_register("autoloader");