<?php
/**
 * 接口列表
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/14
 * +-----------------------------
 * Time: 17:29
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * by 160大药房
 * +-----------------------------
 */

namespace PDDCore\Contract;


interface GoodsInterface
{

    /**
     * 所有数据
     * @return array
     */
    public function getApiParas();

    /**
     * 接口名
     * @return mixed
     */
    public function getApiMethodName();

    /**
     * 验证参数
     * @return mixed
     */
    public function check();

    /**
     * 设置额外参数
     * @param $key
     * @param $value
     * @return mixed
     */
    public function putOuterTextParam($key, $value);
}