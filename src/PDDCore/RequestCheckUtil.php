<?php
/**
 * 参数请求验证基类
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/9
 * +-----------------------------
 * Time: 14:43
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore;


class RequestCheckUtil
{

    /**
     * 验证参数不能为空
     * @param $value 当前参数值
     * @param $fieldName 字段名
     * @throws \Exception
     */
    public static function checkNotNull($value,$fieldName) {

        if(self::checkEmpty($value)){
            throw new \Exception("客户端检查错误：无效参数：缺少必需的参数: " .$fieldName , 40);
        }
    }

    /**
     * 验证参数最大长度
     * @param $value 当前参数
     * @param $maxLength 长度
     * @param $fieldName 字段名
     * @throws \Exception
     */
    public static function checkMaxLength($value,$maxLength,$fieldName){
        if(!self::checkEmpty($value) && mb_strlen($value , "UTF-8") > $maxLength){
            throw new \Exception("客户端检查错误：无效参数：字段长度【 " .$fieldName . " 】大于 " . $maxLength . "." , 41);
        }
    }

    /**
     * 验证参数最小长度
     * @param $value 当前参数
     * @param $minLength 长度
     * @param $fieldName 字段名
     * @throws \Exception
     */
    public static function checkMinLength($value,$minLength,$fieldName){
        if(!self::checkEmpty($value) && mb_strlen($value , "UTF-8") < $minLength){
            throw new \Exception("客户端检查错误：无效参数：字段长度【 " .$fieldName . " 】小于 " . $minLength . "." , 41);
        }
    }

    /**
     * 验证区间范围
     * @param $value 当前参数值
     * @param $minLength 最小长度
     * @param $maxLength 最大长度
     * @param $fieldName 字段名
     * @throws \Exception
     */
    public static function checkBetween($value,$minLength,$maxLength,$fieldName)
    {
        if (self::checkEmpty($value)){
            return ;
        }
        // 验证区间范围
        $valueLength = mb_strlen($value,"UTF-8");
        if ($valueLength <$minLength || $valueLength > $maxLength){
            throw new \Exception("客户端检查错误：无效参数：字段长度【 " .$fieldName . " 】字符长度区间范围{$minLength}~{$maxLength} ." , 41);
        }
    }

    /**
     * 验证数组列表最大长度
     * @param $value 当前参数
     * @param $maxSize 最大长度
     * @param $fieldName 字段列名
     * @throws \Exception
     */
    public static function checkMaxListSize($value,$maxSize,$fieldName) {

        if(self::checkEmpty($value))
            return ;

        $list=preg_split("/,/",$value);
        if(count($list) > $maxSize){
            throw new \Exception("客户端检查错误：无效参数：字段 listsize(字符串分割) 【 ". $fieldName . "】 不能小于 " . $maxSize . " ." , 41);
        }
    }

    /**
     * 验证参数最大数值
     * @param $value 当前参数
     * @param $maxValue 最大参数
     * @param $fieldName 字段列名
     * @throws \Exception
     */
    public static function checkMaxValue($value,$maxValue,$fieldName){

        if(self::checkEmpty($value))
            return ;

        self::checkNumeric($value,$fieldName);

        if($value > $maxValue){
            throw new \Exception("客户端检查错误：无效参数：字段值【" . $fieldName . "】 不能大于" . $maxValue ." ." , 41);
        }
    }

    /**
     * 验证最小参数值
     * @param $value 参数值
     * @param $minValue 最小值
     * @param $fieldName 字段列名
     * @throws \Exception
     */
    public static function checkMinValue($value,$minValue,$fieldName) {

        if(self::checkEmpty($value))
            return ;

        self::checkNumeric($value,$fieldName);

        if($value < $minValue){
            throw new \Exception("客户端检查错误：无效参数：参数字段【 " . $fieldName . "】不能小于 " . $minValue . " ." , 41);
        }
    }

    /**
     * 验证规定范围内
     * @param $value 参数值
     * @param $inValue 区间范围
     * @param $fieldName 字段名
     * @throws \Exception
     */
    public static function checkInValue($value,$inValue,$fieldName)
    {
        if (self::checkEmpty($value)){
            return ;
        }
        if (!in_array($value,$inValue)){
            throw new \Exception("客户端检查错误：无效参数：参数字段【 " . $fieldName . "】不在规定范围内 " . $value . " ." , 41);
        }
    }

    /**
     * 验证是否数字字符串
     * @param $value 参数值
     * @param $fieldName 验证字段
     * @throws \Exception
     */
    protected static function checkNumeric($value,$fieldName) {
        if(!is_numeric($value))
            throw new \Exception("客户端检查错误：无效参数：参数字段【 " . $fieldName . "】非数字信息: " . $value . " ." , 41);
    }

    /**
     * 验证参数是否为空
     * @param $value 参数
     * @return bool
     */
    public static function checkEmpty($value) {
        if(!isset($value))
            return true ;
        if($value === null )
            return true;
        if(is_array($value) && count($value) == 0)
            return true;
        if(is_string($value) && trim($value) === "")
            return true;

        return false;
    }

}