<?php
/**
 * pdd.goods.commit.status.get草稿状态查询接口
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/11
 * +-----------------------------
 * Time: 14:06
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Request;


use PDDCore\Contract\GoodsInterface;
use PDDCore\RequestCheckUtil;

class PddGoodsCommitStatusGetRequest implements GoodsInterface
{
    private $goodsCommitIdList;

    private $apiParas = [];

    /**
     * goods_commit_id列表
     * @param mixed $goodsCommitIdList
     */
    public function setGoodsCommitIdList($goodsCommitIdList)
    {
        $this->goodsCommitIdList = $goodsCommitIdList;
        $this->apiParas['goods_commit_id_list'] = $goodsCommitIdList;
    }

    /**
     * @return array
     */
    public function getApiParas()
    {
        return $this->apiParas;
    }

    public function getApiMethodName()
    {
        return "pdd.goods.commit.status.get";
    }

    public function check()
    {
        RequestCheckUtil::checkNotNull($this->goodsCommitIdList,"goods_commit_id_list");
    }

    public function putOuterTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }

}