<?php
/**
 * pdd.goods.latest.commit.status.get批量goodsId查询最新的审核状态
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/11
 * +-----------------------------
 * Time: 14:13
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Request;


use PDDCore\Contract\GoodsInterface;
use PDDCore\RequestCheckUtil;

class PddGoodsLatestCommitStatusGetRequest implements GoodsInterface
{
    private $goodsIdList;

    private $apiParas = [];

    /**
     * 商品id(不超过100个)
     * @param mixed $goodsIdList
     */
    public function setGoodsIdList($goodsIdList)
    {
        $this->goodsIdList = $goodsIdList;
        $this->apiParas['goods_id_list'] = $goodsIdList;
    }

    /**
     * @return array
     */
    public function getApiParas()
    {
        return $this->apiParas;
    }

    public function getApiMethodName()
    {
        return "pdd.goods.latest.commit.status.get";
    }

    public function check()
    {
        RequestCheckUtil::checkNotNull($this->goodsIdList,"goods_id_list");
    }

    public function putOuterTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }

}