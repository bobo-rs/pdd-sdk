<?php
/**
 * pdd.goods.material.delete商品素材删除接口
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/11
 * +-----------------------------
 * Time: 10:21
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Request;


use PDDCore\Contract\GoodsInterface;
use PDDCore\RequestCheckUtil;

class PddGoodsMaterialDeleteRequest implements GoodsInterface
{
    private $materialId;

    private $apiParas = [];

    /**
     * @param mixed $materialId
     */
    public function setMaterialId($materialId)
    {
        $this->materialId = $materialId;
        $this->apiParas['material_id'] = $materialId;
    }

    /**
     * @return array
     */
    public function getApiParas()
    {
        return $this->apiParas;
    }

    /**
     * @return mixed
     */
    public function getMaterialId()
    {
        return $this->materialId;
    }

    public function getApiMethodName()
    {
        return "pdd.goods.material.delete";
    }

    public function check()
    {
        RequestCheckUtil::checkNotNull($this->materialId,"material_id");
    }

    public function putOuterTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }
}