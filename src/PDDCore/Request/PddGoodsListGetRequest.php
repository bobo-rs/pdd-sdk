<?php
/**
 * pdd.goods.list.get商品列表接口
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/9
 * +-----------------------------
 * Time: 15:52
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Request;


use PDDCore\Contract\GoodsInterface;
use PDDCore\RequestCheckUtil;

class PddGoodsListGetRequest implements GoodsInterface
{
    private $outerId;

    private $isOnsale;

    private $goodsName;

    private $pageSize = 10;

    private $page = 1;

    private $outerGoodsId;

    private $costTempateId;

    private $apiParas = [];

    /**
     * 商品名
     * @param mixed $goodsName
     */
    public function setGoodsName($goodsName)
    {
        $this->goodsName = $goodsName;
        $this->apiParas['goods_name'] = $goodsName;
    }

    /**
     * 页码条数
     * @param int $pageSize
     */
    public function setPageSize($pageSize)
    {
        $this->pageSize = $pageSize;
        $this->apiParas['page_size'] = $pageSize;
    }

    /**
     * 模版id
     * @param mixed $costTempateId
     */
    public function setCostTempateId($costTempateId)
    {
        $this->costTempateId = $costTempateId;
        $this->apiParas['cost_tempate_id'] = $costTempateId;
    }

    /**
     * 上下架状态，0-下架，1-上架
     * @param mixed $isOnsale
     */
    public function setIsOnsale($isOnsale)
    {
        $this->isOnsale = $isOnsale;
        $this->apiParas['is_onsale'] = $isOnsale;
    }

    /**
     * 商家外部商品编码，支持多个，用逗号隔开，最多10个
     * @param mixed $outerGoodsId
     */
    public function setOuterGoodsId($outerGoodsId)
    {
        $this->outerGoodsId = $outerGoodsId;
        $this->apiParas['outer_goods_id'] = $outerGoodsId;
    }

    /**
     * 设置商品外部编码（sku）
     * @param mixed $outerId
     */
    public function setOuterId($outerId)
    {
        $this->outerId = $outerId;
        $this->apiParas['outerId'] = $outerId;
    }

    /**
     * 页码
     * @param int $page
     */
    public function setPage($page)
    {
        $this->page = $page;
        $this->apiParas['page'] = $page;
    }

    /**
     * 接口名
     * @return string
     */
    public function getApiMethodName()
    {
        return "pdd.goods.list.get";
    }

    /**
     * 商品外部编码（sku）
     * @return mixed
     */
    public function getOuterId()
    {
        return $this->outerId;
    }

    /**
     * 获取数据
     * @return array
     */
    public function getApiParas()
    {
        return $this->apiParas;
    }

    /**
     * 验证参数
     */
    public function check()
    {
        RequestCheckUtil::checkMinValue($this->page,1,"page");
        RequestCheckUtil::checkMaxValue($this->pageSize,100,"page_size");
        RequestCheckUtil::checkMinValue($this->pageSize,0,"page_size");
    }

    /**
     * @param $key
     * @param $value
     */
    public function putOuterTextParam($key, $value) {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }
}