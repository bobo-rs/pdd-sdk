<?php
/**
 * pdd.goods.spu.search标品搜索接口
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/11
 * +-----------------------------
 * Time: 11:19
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Request;


use PDDCore\Contract\GoodsInterface;
use PDDCore\RequestCheckUtil;

class PddGoodsSpuSearchRequest implements GoodsInterface
{
    private $catId;

    private $keyProp;

    private $spuName;

    private $apiParas = [];

    /**
     * @param mixed $catId
     */
    public function setCatId($catId)
    {
        $this->catId = $catId;
        $this->apiParas['cat_id'] = $catId;
    }

    /**
     * @param mixed $keyProp
     */
    public function setKeyProp($keyProp)
    {
        $this->keyProp = $keyProp;
        $this->apiParas['key_prop'] = $keyProp;
    }

    /**
     * @param mixed $spuName
     */
    public function setSpuName($spuName)
    {
        $this->spuName = $spuName;
        $this->apiParas['spu_name'] = $spuName;
    }

    /**
     * @return array
     */
    public function getApiParas()
    {
        return $this->apiParas;
    }

    /**
     * @return mixed
     */
    public function getCatId()
    {
        return $this->catId;
    }

    public function getApiMethodName()
    {
        return "pdd.goods.spu.search";
    }

    public function check()
    {
        RequestCheckUtil::checkNotNull($this->catId,"cat_id");
    }

    public function putOuterTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }
}