<?php
/**
 * pdd.goods.video.upload商品视频上传接口
 * 接口地址：https://gw-upload.pinduoduo.com/api/upload
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/11
 * +-----------------------------
 * Time: 9:36
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Request;


use PDDCore\Contract\GoodsInterface;
use PDDCore\RequestCheckUtil;

class PddGoodsVideoUploadRequest implements GoodsInterface
{
    private $file;

    private $apiParas = [];

    /**
     * @param mixed $file
     */
    public function setFile($file)
    {
        $this->file = $file;
        $this->apiParas['file'] = $file;
    }

    /**
     * @return array
     */
    public function getApiParas()
    {
        return $this->apiParas;
    }

    /**
     * 接口
     * @return string
     */
    public function getApiMethodName()
    {
        return "pdd.goods.video.upload";
    }

    /**
     * 验证基础参数
     */
    public function check()
    {
        RequestCheckUtil::checkNotNull($this->file,"file");
    }

    /**
     * 额外参数
     * @param $key
     * @param $value
     */
    public function putOuterTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }
}