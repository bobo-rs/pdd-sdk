<?php
/**
 * pdd.goods.template.property.value.search模板属性值搜索接口
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/11
 * +-----------------------------
 * Time: 10:51
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Request;


use PDDCore\Contract\GoodsInterface;
use PDDCore\RequestCheckUtil;

class PddGoodsTemplatePropertyValueSearchRequest implements GoodsInterface
{
    private $catId;

    private $pageNum;

    private $pageSize;

    private $parentVid;

    private $tempatePid;

    private $value;

    private $refPid;

    private $apiParas = [];

    /**
     * 页面条数
     * @param mixed $pageSize
     */
    public function setPageSize($pageSize)
    {
        $this->pageSize = $pageSize;
        $this->apiParas['page_size'] = $pageSize;
    }

    /**
     * 分类id
     * @param mixed $catId
     */
    public function setCatId($catId)
    {
        $this->catId = $catId;
        $this->apiParas['cat_id'] = $catId;
    }

    /**
     * 页码
     * @param mixed $pageNum
     */
    public function setPageNum($pageNum)
    {
        $this->pageNum = $pageNum;
        $this->apiParas['page_num'] = $pageNum;
    }

    /**
     * 父属性值ID
     * @param mixed $parentVid
     */
    public function setParentVid($parentVid)
    {
        $this->parentVid = $parentVid;
        $this->apiParas['parent_vid'] = $parentVid;
    }

    /**
     * 属性id
     * @param mixed $refPid
     */
    public function setRefPid($refPid)
    {
        $this->refPid = $refPid;
        $this->apiParas['ref_pid'] = $refPid;
    }

    /**
     * 模板属性id，废弃中，请入参属性id
     * @param mixed $tempatePid
     */
    public function setTempatePid($tempatePid)
    {
        $this->tempatePid = $tempatePid;
        $this->apiParas['tempate_pid'] = $tempatePid;
    }

    /**
     * 需要模糊搜索的属性值
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
        $this->apiParas['value'] = $value;
    }

    /**
     * @return array
     */
    public function getApiParas()
    {
        return $this->apiParas;
    }

    public function getApiMethodName()
    {
        return "pdd.goods.template.property.value.search";
    }

    public function check()
    {
        RequestCheckUtil::checkNotNull($this->catId,"cat_id");
        RequestCheckUtil::checkNotNull($this->value,"value");
        RequestCheckUtil::checkNotNull($this->refPid,"ref_pid");
        RequestCheckUtil::checkMaxValue($this->pageSize,500,"page_size");
        RequestCheckUtil::checkMinValue($this->pageNum,1,"page_num");
    }

    public function putOuterTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }

}