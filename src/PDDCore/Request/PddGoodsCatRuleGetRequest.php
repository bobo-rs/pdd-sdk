<?php
/**
 * pdd.goods.cat.rule.get类目商品发布规则查询接口
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/9
 * +-----------------------------
 * Time: 17:30
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Request;

use PDDCore\Contract\GoodsInterface;
use PDDCore\RequestCheckUtil;

class PddGoodsCatRuleGetRequest implements GoodsInterface
{
    private $catId;

    private $apiParas=[];

    /**
     * @param mixed $catId
     */
    public function setCatId($catId)
    {
        $this->catId = $catId;
        $this->apiParas['cat_id'] = $catId;
    }

    public function getApiMethodName()
    {
        return "pdd.goods.cat.rule.get";
    }

    /**
     * @return array
     */
    public function getApiParas()
    {
        return $this->apiParas;
    }

    /**
     * 验证基础
     */
    public function check()
    {
        RequestCheckUtil::checkNotNull($this->catId,"cat_id");
    }

    public function putOuterTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }
}