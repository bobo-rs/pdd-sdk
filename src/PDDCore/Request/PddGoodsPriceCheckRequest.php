<?php
/**
 *
 * +-----------------------------
 * Class name: PddGoodsPriceCheckRequest
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2023/5/9
 * +-----------------------------
 * Time: 10:23
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * by PHPstorm
 * +-----------------------------
 */

namespace PDDCore\Request;


use PDDCore\Contract\GoodsInterface;
use PDDCore\RequestCheckUtil;

class PddGoodsPriceCheckRequest implements GoodsInterface
{

    private $apiParas = [];

    private $goodsId;

    /**
     * @param mixed $goodsId
     */
    public function setGoodsId($goodsId): void
    {
        $this->goodsId = $goodsId;
        $this->apiParas['goodsId'] = $goodsId;
    }

    /**
     * @return mixed
     */
    public function getGoodsId()
    {
        return $this->goodsId;
    }

    public function check()
    {
        // TODO: Implement check() method.
        RequestCheckUtil::checkNotNull($this->goodsId, 'goodsId');
    }
    public function getApiMethodName()
    {
        // TODO: Implement getApiMethodName() method.
        return "pdd.goods.price.check";
    }
    public function getApiParas()
    {
        // TODO: Implement getApiParas() method.
        return $this->apiParas;
    }
    public function putOuterTextParam($key, $value)
    {
        // TODO: Implement putOuterTextParam() method.
        $this->$key = $value;
        $this->apiParas[$key] = $value;
    }
}
