<?php
/**
 * pdd.goods.detail.get商品明细
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/9
 * +-----------------------------
 * Time: 17:24
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Request;


use PDDCore\Contract\GoodsInterface;
use PDDCore\RequestCheckUtil;

class PddGoodsDetailGetRequest implements GoodsInterface
{
    private $goodsId;

    private $apiParas=[];

    /**
     * @param mixed $goodsId
     */
    public function setGoodsId($goodsId)
    {
        $this->goodsId = $goodsId;
        $this->apiParas['goods_id'] = $goodsId;
    }

    /**
     * @return mixed
     */
    public function getGoodsId()
    {
        return $this->goodsId;
    }

    /**
     * @return array
     */
    public function getApiParas()
    {
        return $this->apiParas;
    }

    /**
     * 接口名
     * @return string
     */
    public function getApiMethodName()
    {
        return "pdd.goods.detail.get";
    }

    /**
     * 验证基础
     */
    public function check()
    {
        RequestCheckUtil::checkNotNull($this->goodsId,"goods_id");
    }

    /**
     * @param $key
     * @param $value
     */
    public function putOuterTextParam($key, $value) {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }
}