<?php
/**
 * pdd.goods.commit.detail.get获取商品提交的商品详情
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/11
 * +-----------------------------
 * Time: 13:51
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Request;


use PDDCore\Contract\GoodsInterface;
use PDDCore\RequestCheckUtil;

class PddGoodsCommitDetailGetRequest implements GoodsInterface
{
    private $goodsCommitId;

    private $goodsId;

    private $apiParas = [];

    /**
     * 商品id
     * @param mixed $goodsId
     */
    public function setGoodsId($goodsId)
    {
        $this->goodsId = $goodsId;
        $this->apiParas['goods_id'] = $goodsId;
    }

    /**
     * 提交申请的序列id
     * @param mixed $goodsCommitId
     */
    public function setGoodsCommitId($goodsCommitId)
    {
        $this->goodsCommitId = $goodsCommitId;
        $this->apiParas['goods_commit_id'] = $goodsCommitId;
    }

    /**
     * 接口名
     * @return array
     */
    public function getApiParas()
    {
        return $this->apiParas;
    }

    public function getApiMethodName()
    {
        return "pdd.goods.commit.detail.get";
    }

    public function check()
    {
        RequestCheckUtil::checkNotNull($this->goodsId,"goods_id");
        RequestCheckUtil::checkNotNull($this->goodsCommitId,"goods_commit_id");
    }

    public function putOuterTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }
}