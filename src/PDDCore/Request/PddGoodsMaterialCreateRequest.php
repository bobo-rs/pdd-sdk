<?php
/**
 * pdd.goods.material.create商品素材创建接口
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/11
 * +-----------------------------
 * Time: 9:49
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Request;


use PDDCore\Contract\GoodsInterface;
use PDDCore\RequestCheckUtil;

class PddGoodsMaterialCreateRequest implements GoodsInterface
{
    private $content;

    private $fileId;

    private $goodsId;

    private $materialType;

    private $apiParas=[];

    /**
     * @param mixed $goodsId
     */
    public function setGoodsId($goodsId)
    {
        $this->goodsId = $goodsId;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @param mixed $fileId
     */
    public function setFileId($fileId)
    {
        $this->fileId = $fileId;
    }

    /**
     * @param mixed $materialType
     */
    public function setMaterialType($materialType)
    {
        $this->materialType = $materialType;
    }

    /**
     * @return array
     */
    public function getApiParas()
    {
        return $this->apiParas;
    }

    /**
     * @return mixed
     */
    public function getFileId()
    {
        return $this->fileId;
    }

    /**
     * @return mixed
     */
    public function getGoodsId()
    {
        return $this->goodsId;
    }

    /**
     * @return mixed
     */
    public function getMaterialType()
    {
        return $this->materialType;
    }

    public function getApiMethodName()
    {
        return "pdd.goods.material.create";
    }

    public function check()
    {
        RequestCheckUtil::checkNotNull($this->fileId,"file_id");
        RequestCheckUtil::checkNotNull($this->goodsId,"goods_id");
        RequestCheckUtil::checkNotNull($this->content,"content");
        RequestCheckUtil::checkNotNull($this->materialType,"material_type");
        RequestCheckUtil::checkInValue($this->materialType,[1,3],"material_type");
    }

    public function putOuterTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }
}