<?php
/**
 * pdd.goods.logistics.template.get商品运费模版接口
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/11
 * +-----------------------------
 * Time: 13:44
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Request;


use PDDCore\Contract\GoodsInterface;
use PDDCore\RequestCheckUtil;

class PddGoodsLogisticsTemplateGetRequest implements GoodsInterface
{
    private $page;

    private $pageSize;

    private $apiParas = [];

    /**
     * 默认返回运费模板的页数为1，最高为100页，注意：page与page_size必须传一个
     * @param mixed $page
     */
    public function setPage($page)
    {
        $this->page = $page;
        $this->apiParas['page'] = $page;
    }

    /**
     * 默认返回20条模板数据，最多100条数据
     * @param mixed $pageSize
     */
    public function setPageSize($pageSize)
    {
        $this->pageSize = $pageSize;
        $this->apiParas['page_size'] = $pageSize;
    }

    /**
     * @return array
     */
    public function getApiParas()
    {
        return $this->apiParas;
    }

    public function getApiMethodName()
    {
        return "pdd.goods.logistics.template.get";
    }

    public function check()
    {
        RequestCheckUtil::checkNotNull($this->page,"page");
        RequestCheckUtil::checkNotNull($this->pageSize,"page_size");
        RequestCheckUtil::checkMaxValue($this->pageSize,100,"page_size");
        RequestCheckUtil::checkMinValue($this->page,1,"page");
    }

    public function putOuterTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }
}