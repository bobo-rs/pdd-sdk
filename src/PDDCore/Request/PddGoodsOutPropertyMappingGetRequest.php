<?php
/**
 * pdd.goods.out.property.mapping.get站内外属性映射接口
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/11
 * +-----------------------------
 * Time: 11:03
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Request;


use PDDCore\Contract\GoodsInterface;
use PDDCore\RequestCheckUtil;

class PddGoodsOutPropertyMappingGetRequest implements GoodsInterface
{
    private $catId;

    private $outPropertyName;

    private $outPropertyValueName;

    private $apiParas = [];

    /**
     * 拼多多叶子类目id
     * @param mixed $catId
     */
    public function setCatId($catId)
    {
        $this->catId = $catId;
        $this->apiParas['cat_id'] = $catId;
    }

    /**
     * 外部平台属性名称
     * @param mixed $outPropertyName
     */
    public function setOutPropertyName($outPropertyName)
    {
        $this->outPropertyName = $outPropertyName;
        $this->apiParas['out_property_name'] = $outPropertyName;
    }

    /**
     * 外部平台属性值名称
     * @param mixed $outPropertyValueName
     */
    public function setOutPropertyValueName($outPropertyValueName)
    {
        $this->outPropertyValueName = $outPropertyValueName;
        $this->apiParas['out_property_value_name'] = $outPropertyValueName;
    }

    /**
     * @return array
     */
    public function getApiParas()
    {
        return $this->apiParas;
    }

    public function getApiMethodName()
    {
        return "pdd.goods.out.property.mapping.get";
    }

    public function check()
    {
        RequestCheckUtil::checkNotNull($this->catId,"cat_id");
        RequestCheckUtil::checkNotNull($this->outPropertyName,"out_property_name");
    }

    public function putOuterTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }
}