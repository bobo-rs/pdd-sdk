<?php
/**
 * pdd.goods.sku.price.update修改商品sku价格
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/11
 * +-----------------------------
 * Time: 14:22
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Request;


use PDDCore\Contract\GoodsInterface;
use PDDCore\RequestCheckUtil;

class PddGoodsSkuPriceUpdateRequest implements GoodsInterface
{
    private $goodsId;

    private $marketPrice;

    private $marketPriceInYuan;

    private $skuPriceList;

    private $syncGoodsOperate;

    private $ignoreEditWarn;

    private $apiParas = [];

    /**
     * 商品id
     * @param mixed $goodsId
     */
    public function setGoodsId($goodsId)
    {
        $this->goodsId = $goodsId;
        $this->apiParas['goods_id'] = $goodsId;
    }

    /**
     * @param mixed $ignoreEditWarn
     */
    public function setIgnoreEditWarn($ignoreEditWarn)
    {
        $this->ignoreEditWarn = $ignoreEditWarn;
        $this->apiParas['ignore_edit_warn'] = $ignoreEditWarn;
    }

    /**
     * 参考价 （单位分）
     * @param mixed $marketPrice
     */
    public function setMarketPrice($marketPrice)
    {
        $this->marketPrice = $marketPrice;
        $this->apiParas['market_price'] = $marketPrice;
    }

    /**
     * 参考价 （单位元）
     * @param mixed $marketPriceInYuan
     */
    public function setMarketPriceInYuan($marketPriceInYuan)
    {
        $this->marketPriceInYuan = $marketPriceInYuan;
        $this->apiParas['market_price_in_yuan'] = $marketPriceInYuan;
    }

    /**
     * 待修改的sku价格
     * @param mixed $skuPriceList
     */
    public function setSkuPriceList($skuPriceList)
    {
        $this->skuPriceList = $skuPriceList;
        $this->apiParas['sku_price_list'] = $skuPriceList;
    }

    /**
     * 提交后上架状态，0:上架,1:保持原样
     * @param mixed $syncGoodsOperate
     */
    public function setSyncGoodsOperate($syncGoodsOperate)
    {
        $this->syncGoodsOperate = $syncGoodsOperate;
        $this->apiParas['sync_goods_operate'] = $syncGoodsOperate;
    }

    /**
     * @return array
     */
    public function getApiParas()
    {
        return $this->apiParas;
    }

    public function getApiMethodName()
    {
        // TODO: Implement getApiMethodName() method.
        return "pdd.goods.sku.price.update";
    }

    public function check()
    {
        RequestCheckUtil::checkNotNull($this->goodsId,"goods_id");
        RequestCheckUtil::checkNotNull($this->skuPriceList,"sku_price_list");
    }

    public function putOuterTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }

}
