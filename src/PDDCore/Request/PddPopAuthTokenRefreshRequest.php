<?php
/**
 * pdd.pop.auth.token.refresh刷新Access Token
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/14
 * +-----------------------------
 * Time: 16:58
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * by 160大药房
 * +-----------------------------
 */

namespace PDDCore\Request;


use PDDCore\Contract\GoodsInterface;
use PDDCore\RequestCheckUtil;

class PddPopAuthTokenRefreshRequest implements GoodsInterface
{
    private $refreshToken;

    private $apiParas = [];

    /**
     * @param mixed $refreshToken
     */
    public function setRefreshToken($refreshToken)
    {
        $this->refreshToken = $refreshToken;
        $this->apiParas['refresh_token'] = $refreshToken;
    }

    /**
     * 所有数据
     * @return array
     */
    public function getApiParas()
    {
        return $this->apiParas;
    }

    /**
     * 接口名
     * @return string
     */
    public function getApiMethodName()
    {
        return "pdd.pop.auth.token.refresh";
    }

    public function check()
    {
        RequestCheckUtil::checkNotNull($this->refreshToken,"refresh_token");
    }

    public function putOuterTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }
}