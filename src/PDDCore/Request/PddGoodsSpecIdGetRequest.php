<?php
/**
 * pdd.goods.spec.id.get生成商家自定义的规格
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/11
 * +-----------------------------
 * Time: 13:37
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Request;


use PDDCore\Contract\GoodsInterface;
use PDDCore\RequestCheckUtil;

class PddGoodsSpecIdGetRequest implements GoodsInterface
{
    private $parentSpecId;

    private $specName;

    private $apiParas = [];

    /**
     * 拼多多标准规格ID，可以通过pdd.goods.spec.get接口获取
     * @param mixed $parentSpecId
     */
    public function setParentSpecId($parentSpecId)
    {
        $this->parentSpecId = $parentSpecId;
        $this->apiParas['parent_spec_id'] = $parentSpecId;
    }

    /**
     * 商家编辑的规格值，如颜色规格下设置白色属性
     * @param mixed $specName
     */
    public function setSpecName($specName)
    {
        $this->specName = $specName;
        $this->apiParas['spec_name'] = $specName;
    }

    /**
     * @return array
     */
    public function getApiParas()
    {
        return $this->apiParas;
    }

    public function getApiMethodName()
    {
        return "pdd.goods.spec.id.get";
    }

    public function check()
    {
        RequestCheckUtil::checkNotNull($this->parentSpecId,"parent_spec_id");
        RequestCheckUtil::checkNotNull($this->specName,"spec_name");
    }

    public function putOuterTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }

}