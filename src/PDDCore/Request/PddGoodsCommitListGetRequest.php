<?php
/**
 * pdd.goods.commit.list.get草稿列表接口
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/11
 * +-----------------------------
 * Time: 13:56
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Request;


use PDDCore\Contract\GoodsInterface;
use PDDCore\RequestCheckUtil;

class PddGoodsCommitListGetRequest implements GoodsInterface
{
    private $checkStatus;

    private $goodsId;

    private $page;

    private $pageSize;

    private $apiParas = [];

    /**
     * 商品id
     * @param mixed $goodsId
     */
    public function setGoodsId($goodsId)
    {
        $this->goodsId = $goodsId;
        $this->apiParas['goods_id'] = $goodsId;
    }

    /**
     * 每页数量，最多不超过100
     * @param mixed $pageSize
     */
    public function setPageSize($pageSize)
    {
        $this->pageSize = $pageSize;
        $this->apiParas['page_size'] = $pageSize;
    }

    /**
     * 页码，最多不超过100
     * @param mixed $page
     */
    public function setPage($page)
    {
        $this->page = $page;
        $this->apiParas['page'] = $page;
    }

    /**
     * 草稿状态（0:编辑中,1:审核中,2:审核通过,3:审核驳回）
     * @param mixed $checkStatus
     */
    public function setCheckStatus($checkStatus)
    {
        $this->checkStatus = $checkStatus;
        $this->apiParas['check_status'] = $checkStatus;
    }

    /**
     * @return array
     */
    public function getApiParas()
    {
        return $this->apiParas;
    }

    /**
     * @return mixed
     */
    public function getGoodsId()
    {
        return $this->goodsId;
    }

    public function getApiMethodName()
    {
        return "pdd.goods.commit.list.get";
    }

    public function check()
    {
        RequestCheckUtil::checkNotNull($this->page,"page");
        RequestCheckUtil::checkNotNull($this->pageSize,"page_size");
        RequestCheckUtil::checkInValue($this->checkStatus,[0,1,2,3],"check_status");
        RequestCheckUtil::checkMaxValue($this->pageSize,100,"page_size");
        RequestCheckUtil::checkMaxValue($this->page,100,"page");
    }

    public function putOuterTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }
}