<?php
/**
 * pdd.goods.cats.get商品标准类目接口
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/9
 * +-----------------------------
 * Time: 17:26
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Request;


use PDDCore\Contract\GoodsInterface;
use PDDCore\RequestCheckUtil;

class PddGoodsCatsGetRequest implements GoodsInterface
{
    private $parentCatId;

    private $apiParas=[];

    /**
     * @param mixed $parentCatId
     */
    public function setParentCatId($parentCatId)
    {
        $this->parentCatId = $parentCatId;
        $this->apiParas['parent_cat_id'] = $parentCatId;
    }

    /**
     * @return array
     */
    public function getApiParas()
    {
        return $this->apiParas;
    }

    /**
     * @return mixed
     */
    public function getParentCatId()
    {
        return $this->parentCatId;
    }

    /**
     * 接口名
     * @return string
     */
    public function getApiMethodName()
    {
        return "pdd.goods.cats.get";
    }

    /**
     * 验证基础
     */
    public function check()
    {
        RequestCheckUtil::checkNotNull($this->parentCatId,"parent_cat_id");
    }

    /**
     * @param $key
     * @param $value
     */
    public function putOuterTextParam($key, $value) {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }
}