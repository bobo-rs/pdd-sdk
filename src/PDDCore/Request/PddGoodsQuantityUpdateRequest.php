<?php
/**
 * pdd.goods.quantity.update商品库存更新接口
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/9
 * +-----------------------------
 * Time: 18:02
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Request;


use PDDCore\Contract\GoodsInterface;
use PDDCore\RequestCheckUtil;

class PddGoodsQuantityUpdateRequest implements GoodsInterface
{
    private $goodsId;

    private $quantity;

    private $skuId;

    private $outerId;

    private $updateType;

    private $apiParas=[];

    /**
     * sku商家编码，如果sku_id未填，则使用outer_id
     * @param mixed $outerId
     */
    public function setOuterId($outerId)
    {
        $this->outerId = $outerId;
        $this->apiParas['outer_id'] = $outerId;
    }

    /**
     * 商品id
     * @param mixed $goodsId
     */
    public function setGoodsId($goodsId)
    {
        $this->goodsId = $goodsId;
        $this->apiParas['goods_id'] = $goodsId;
    }

    /**
     * 库存修改值。当全量更新库存时，quantity必须为大于等于0的正整数；
     * 当增量更新库存时，quantity为整数，可小于等于0。
     * 若增量更新时传入的库存为负数，则负数与实际库存之和不能小于0。
     * 比如当前实际库存为1，传入增量更新quantity=-1，库存改为0
     * @param mixed $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        $this->apiParas['quantity'] = $quantity;
    }

    /**
     * 库存更新方式，可选。1为全量更新，2为增量更新。如果不填，默认为全量更新
     * @param mixed $updateType
     */
    public function setUpdateType($updateType)
    {
        $this->updateType = $updateType;
        $this->apiParas['update_type'] = $updateType;
    }

    /**
     * sku_id和outer_id必填一个，优先使用sku_id
     * @param mixed $skuId
     */
    public function setSkuId($skuId)
    {
        $this->skuId = $skuId;
        $this->apiParas['sku_id'] = $skuId;
    }

    /**
     * @return array
     */
    public function getApiParas()
    {
        return $this->apiParas;
    }

    /**
     * @return mixed
     */
    public function getSkuId()
    {
        return $this->skuId;
    }

    /**
     * @return mixed
     */
    public function getGoodsId()
    {
        return $this->goodsId;
    }

    /**
     * @return mixed
     */
    public function getOuterId()
    {
        return $this->outerId;
    }

    /**
     * 接口名
     * @return string
     */
    public function getApiMethodName()
    {
        return "pdd.goods.quantity.update";
    }

    /**
     * 验证基础参数
     */
    public function check()
    {
        RequestCheckUtil::checkNotNull($this->goodsId,"goods_id");
        RequestCheckUtil::checkNotNull($this->quantity,"quantity");
        RequestCheckUtil::checkMinValue($this->updateType,[1,2],"update_type");
    }

    /**
     * 额外参数
     * @param $key
     * @param $value
     */
    public function putOuterTextParam($key,$value)
    {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }
}