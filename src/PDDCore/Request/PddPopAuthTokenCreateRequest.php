<?php
/**
 * pdd.pop.auth.token.create获取Access Token
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/14
 * +-----------------------------
 * Time: 16:51
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * by 160大药房
 * +-----------------------------
 */

namespace PDDCore\Request;


use PDDCore\Contract\GoodsInterface;
use PDDCore\RequestCheckUtil;

class PddPopAuthTokenCreateRequest implements GoodsInterface
{
    private $code;

    private $apiParas = [];

    /**
     * 页面授权临时code
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
        $this->apiParas['code'] = $code;
    }

    /**
     * 所有数据
     * @return array
     */
    public function getApiParas()
    {
        return $this->apiParas;
    }

    /**
     * 接口名
     * @return string
     */
    public function getApiMethodName()
    {
        return "pdd.pop.auth.token.create";
    }

    public function check()
    {
        RequestCheckUtil::checkNotNull($this->code,"code");
    }

    public function putOuterTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }
}