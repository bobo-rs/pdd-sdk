<?php
/**
 * pdd.gooods.sku.measurement.list商品sku计量单位枚举
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/11
 * +-----------------------------
 * Time: 17:19
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Request;


use PDDCore\Contract\GoodsInterface;

class PddGoodsSkuMeasurementListRequest implements GoodsInterface
{
    private $apiParas = [];

    /**
     * @return array
     */
    public function getApiParas()
    {
        return $this->apiParas;
    }

    /**
     * 接口名
     * @return string
     */
    public function getApiMethodName()
    {
        return "pdd.gooods.sku.measurement.list";
    }

    public function check()
    {

    }

    /**
     * 添加外部参数
     * @param $key
     * @param $value
     */
    public function putOuterTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }
}