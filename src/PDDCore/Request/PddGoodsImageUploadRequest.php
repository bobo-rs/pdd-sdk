<?php
/**
 * pdd.goods.image.upload商品图片上传接口
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/9
 * +-----------------------------
 * Time: 17:18
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Request;


use PDDCore\Contract\GoodsInterface;
use PDDCore\RequestCheckUtil;

class PddGoodsImageUploadRequest implements GoodsInterface
{
    private $image;

    private $apiParas=[];

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
        $this->apiParas['image'] = $image;
    }

    /**
     * @return array
     */
    public function getApiParas()
    {
        return $this->apiParas;
    }

    /**
     * 接口名
     * @return string
     */
    public function getApiMethodName()
    {
        return "pdd.goods.image.upload";
    }

    /**
     * 验证参数
     */
    public function check()
    {
        RequestCheckUtil::checkNotNull($this->image,"image");
    }

    /**
     * @param $key
     * @param $value
     */
    public function putOuterTextParam($key, $value) {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }
}