<?php
/**
 * pdd.goods.material.query商品素材列表查询
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/11
 * +-----------------------------
 * Time: 10:28
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Request;


use PDDCore\Contract\GoodsInterface;
use PDDCore\RequestCheckUtil;

class PddGoodsMaterialQueryRequest implements GoodsInterface
{
    private $goodsIdList;

    private $typeList;

    private $apiParas = [];

    /**
     * 设置商品id列表
     * @param mixed $goodsIdList
     */
    public function setGoodsIdList($goodsIdList)
    {
        $this->goodsIdList = $goodsIdList;
        $this->apiParas['goods_id_list'] = $goodsIdList;
    }

    /**
     * 设置素材类型列表
     * @param mixed $typeList
     */
    public function setTypeList($typeList)
    {
        $this->typeList = $typeList;
        $this->apiParas['type_list'] = $typeList;
    }

    /**
     * @return array
     */
    public function getApiParas()
    {
        return $this->apiParas;
    }

    public function getApiMethodName()
    {
        return "pdd.goods.material.query";
    }

    public function check()
    {
        RequestCheckUtil::checkNotNull($this->goodsIdList,"goods_id_list");
        RequestCheckUtil::checkNotNull($this->typeList,"type_list");
    }

    public function putOuterTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }
}