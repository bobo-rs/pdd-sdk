<?php
/**
 * pdd.one.express.cost.template按id获取商品运费模版接口
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/14
 * +-----------------------------
 * Time: 17:47
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * by 160大药房
 * +-----------------------------
 */

namespace PDDCore\Request;


use PDDCore\Contract\GoodsInterface;
use PDDCore\RequestCheckUtil;

class PddOneExpressCostTemplateRequest implements GoodsInterface
{
    private $costTemplateId;

    private $apiParas = [];

    /**
     * 运费模板id
     * @param mixed $costTemplateId
     */
    public function setCostTemplateId($costTemplateId)
    {
        $this->costTemplateId = $costTemplateId;
        $this->apiParas['cost_template_id'] = $costTemplateId;
    }

    public function getApiParas()
    {
        // TODO: Implement getApiParas() method.
        return $this->apiParas;
    }

    public function getApiMethodName()
    {
        // TODO: Implement getApiMethodName() method.
        return "pdd.one.express.cost.template";
    }

    public function check()
    {
        // TODO: Implement check() method.
        RequestCheckUtil::checkNotNull($this->costTemplateId,"cost_template_id");
    }

    public function putOuterTextParam($key, $value)
    {
        // TODO: Implement putOuterTextParam() method.
    }
}