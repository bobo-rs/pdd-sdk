<?php
/**
 * pdd.goods.edit.goods.commit新增或编辑草稿接口
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/11
 * +-----------------------------
 * Time: 15:00
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Request;


use PDDCore\Contract\GoodsInterface;
use PDDCore\RequestCheckUtil;

class PddGoodsEditGoodsCommitRequest implements GoodsInterface
{
    private $badFruitClaim;

    private $buyLimit;

    private $carouselGallery;

    private $carouselVideo;

    private $carouselVideoUrl;

    private $catId;

    private $costTemplateId;

    private $countryId;

    private $customerNum;

    private $customs;

    private $deliveryOneDay;

    private $detailGallery;

    private $elecGoodsAttributes;

    private $goodsDesc;

    private $goodsId;

    private $goodsName;

    private $goodsProperties;

    private $goodsTradeAttr;

    private $goodsTravelAttr;

    private $goodsType;

    private $imageUrl;

    private $invoiceStatus;

    private $isCustoms;

    private $isFolt;

    private $isPreSale;

    private $isRefundable;

    private $lackOfWeightClaim;

    private $maiJiaZiTi;

    private $marketPrice;

    private $operateType;

    private $orderLimit;

    private $originCountryId;

    private $outGoodsId;

    private $outSourceGoodsId;

    private $outSourceType;

    private $overseaGoods;

    private $overseaType;

    private $preSaleTime;

    private $quanGuoLianBao;

    private $secondHand;

    private $shangMenAnZhuang;

    private $shipmentLimitSecond;

    private $sizeSpecId;

    private $skuList;

    private $skuType;

    private $songHuoAnZhuang;

    private $songHuoRuHu;

    private $syncGoodsOperate;

    private $tinyName;

    private $warehouse;

    private $warmTips;

    private $zhiHuanBuXiu;

    private $deliveryType;

    private $isGroupPreSale;

    private $isSkuPreSale;

    private $ignoreEditWarn;

    private $goodsCommitId;

    private $apiParas = [];

    /**
     * 提交后上架状态，0:上架,1:保持原样
     * @param mixed $syncGoodsOperate
     */
    public function setSyncGoodsOperate($syncGoodsOperate)
    {
        $this->syncGoodsOperate = $syncGoodsOperate;
        $this->apiParas['sync_goods_operate'] = $syncGoodsOperate;
    }

    /**
     * 参考价格，单位为分
     * @param mixed $marketPrice
     */
    public function setMarketPrice($marketPrice)
    {
        $this->marketPrice = $marketPrice;
        $this->apiParas['market_price'] = $marketPrice;
    }

    /**
     * 是否获取商品发布警告信息，默认为忽略
     * @param mixed $ignoreEditWarn
     */
    public function setIgnoreEditWarn($ignoreEditWarn)
    {
        $this->ignoreEditWarn = $ignoreEditWarn;
        $this->apiParas['ignore_edit_warn'] = $ignoreEditWarn;
    }

    /**
     * 商品ID
     * @param mixed $goodsId
     */
    public function setGoodsId($goodsId)
    {
        $this->goodsId = $goodsId;
        $this->apiParas['goods_id'] = $goodsId;
    }

    /**
     * 叶子类目ID
     * @param mixed $catId
     */
    public function setCatId($catId)
    {
        $this->catId = $catId;
        $this->apiParas['cat_id'] = $catId;
    }

    /**
     * 商品名
     * @param mixed $goodsName
     */
    public function setGoodsName($goodsName)
    {
        $this->goodsName = $goodsName;
        $this->apiParas['goods_name'] = $goodsName;
    }

    /**
     * 1-国内普通商品，2-进口，3-国外海淘，4-直邮 ,5-流量,6-话费,7,优惠券;8-QQ充值,9-加油卡，15-商家卡券，19-平台卡券，暂时支持1-普通商品的上架 19-平台卡券
     * @param mixed $goodsType
     */
    public function setGoodsType($goodsType)
    {
        $this->goodsType = $goodsType;
        $this->apiParas['goods_type'] = $goodsType;
    }

    /**
     * 商品主图，请参考拼多多首页大图，如果商品参加部分活动则必填，否则无法参加活动 a. 尺寸750 x 352px b. 大小100k以内 c. 图片格式仅支持JPG,PNG格式 d. 图片背景应以纯白为主, 商品图案居中显示 e. 图片不可以添加任何品牌相关文字或logo
     * @param mixed $imageUrl
     */
    public function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;
        $this->apiParas['image_url'] = $imageUrl;
    }

    /**
     * 坏果包赔
     * @param mixed $badFruitClaim
     */
    public function setBadFruitClaim($badFruitClaim)
    {
        $this->badFruitClaim = $badFruitClaim;
        $this->apiParas['bad_fruit_claim'] = $badFruitClaim;
    }

    /**
     * 限购次数
     * @param mixed $buyLimit
     */
    public function setBuyLimit($buyLimit)
    {
        $this->buyLimit = $buyLimit;
        $this->apiParas['buy_limit'] = $buyLimit;
    }

    /**
     * 商品轮播图，按次序上传，图片格式支持JPEG/JPG/PNG， 图片尺寸长宽比1：1且尺寸不低于480px，图片大小最高1MB
     * @param mixed $carouselGallery
     */
    public function setCarouselGallery($carouselGallery)
    {
        $this->carouselGallery = $carouselGallery;
        $this->apiParas['carousel_gallery'] = $carouselGallery;
    }

    /**
     * 商品视频
     * @param mixed $carouselVideo
     */
    public function setCarouselVideo($carouselVideo)
    {
        $this->carouselVideo = $carouselVideo;
        $this->apiParas['carousel_video'] = $carouselVideo;
    }

    /**
     * 商品轮播视频
     * @param mixed $carouselVideoUrl
     */
    public function setCarouselVideoUrl($carouselVideoUrl)
    {
        $this->carouselVideoUrl = $carouselVideoUrl;
        $this->apiParas['carousel_video_url'] = $carouselVideoUrl;
    }

    /**
     * 物流运费模板ID，可使用pdd.logistics.template.get获取
     * @param mixed $costTemplateId
     */
    public function setCostTemplateId($costTemplateId)
    {
        $this->costTemplateId = $costTemplateId;
        $this->apiParas['cost_template_id'] = $costTemplateId;
    }

    /**
     * 地区/国家ID，0-中国，暂时只传0（普通商品）
     * @param mixed $countryId
     */
    public function setCountryId($countryId)
    {
        $this->countryId = $countryId;
        $this->apiParas['country_id'] = $countryId;
    }

    /**
     * 团购人数
     * @param mixed $customerNum
     */
    public function setCustomerNum($customerNum)
    {
        $this->customerNum = $customerNum;
        $this->apiParas['customer_num'] = $customerNum;
    }

    /**
     * 海关名称，只在goods_type为直供商品时有效（现阶段暂不支持）
     * @param mixed $customs
     */
    public function setCustoms($customs)
    {
        $this->customs = $customs;
        $this->apiParas['customs'] = $customs;
    }

    /**
     * 是否当日发货
     * @param mixed $deliveryOneDay
     */
    public function setDeliveryOneDay($deliveryOneDay)
    {
        $this->deliveryOneDay = $deliveryOneDay;
        $this->apiParas['delivery_one_day'] = $deliveryOneDay;
    }

    /**
     * 发货方式。0：无物流发货；1：有物流发货。
     * @param mixed $deliveryType
     */
    public function setDeliveryType($deliveryType)
    {
        $this->deliveryType = $deliveryType;
        $this->apiParas['delivery_type'] = $deliveryType;
    }

    /**
     * 商品详情图： a. 尺寸要求宽度处于480~1200px之间，高度0-1500px之间 b. 大小1M以内 c. 数量限制在20张之间 d. 图片格式仅支持JPG,PNG格式 e. 点击上传时，支持批量上传详情图
     * @param mixed $detailGallery
     */
    public function setDetailGallery($detailGallery)
    {
        $this->detailGallery = $detailGallery;
        $this->apiParas['detail_gallery'] = $detailGallery;
    }

    /**
     * 卡券类商品属性
     * @param mixed $elecGoodsAttributes
     */
    public function setElecGoodsAttributes($elecGoodsAttributes)
    {
        $this->elecGoodsAttributes = $elecGoodsAttributes;
        $this->apiParas['elec_goods_attributes'] = $elecGoodsAttributes;
    }

    /**
     * 商品描述， 字数限制：20-500，例如，新包装，保证产品的口感和新鲜度。单颗独立小包装，双重营养，1斤家庭分享装，更实惠新疆一级骏枣夹核桃仁。
     * @param mixed $goodsDesc
     */
    public function setGoodsDesc($goodsDesc)
    {
        $this->goodsDesc = $goodsDesc;
        $this->apiParas['goods_desc'] = $goodsDesc;
    }

    /**
     * 商品属性列表
     * @param mixed $goodsProperties
     */
    public function setGoodsProperties($goodsProperties)
    {
        $this->goodsProperties = $goodsProperties;
        $this->apiParas['goods_properties'] = $goodsProperties;
    }

    /**
     * 日历商品交易相关信息
     * @param mixed $goodsTradeAttr
     */
    public function setGoodsTradeAttr($goodsTradeAttr)
    {
        $this->goodsTradeAttr = $goodsTradeAttr;
        $this->apiParas['goods_trade_attr'] = $goodsTradeAttr;
    }

    /**
     * 商品出行信息
     * @param mixed $goodsTravelAttr
     */
    public function setGoodsTravelAttr($goodsTravelAttr)
    {
        $this->goodsTravelAttr = $goodsTravelAttr;
        $this->apiParas['goods_travel_attr'] = $goodsTravelAttr;
    }

    /**
     * 是否支持开票（测试中）
     * @param mixed $invoiceStatus
     */
    public function setInvoiceStatus($invoiceStatus)
    {
        $this->invoiceStatus = $invoiceStatus;
        $this->apiParas['invoice_status'] = $invoiceStatus;
    }

    /**
     * 是否需要上报海关，现阶段入参默认false，入参true会失败
     * @param mixed $isCustoms
     */
    public function setIsCustoms($isCustoms)
    {
        $this->isCustoms = $isCustoms;
        $this->apiParas['is_customs'] = $isCustoms;
    }

    /**
     * 是否支持假一赔十，false-不支持，true-支持
     * @param mixed $isFolt
     */
    public function setIsFolt($isFolt)
    {
        $this->isFolt = $isFolt;
        $this->apiParas['is_folt'] = $isFolt;
    }

    /**
     * 是否成团预售。0：不是；1:是。
     * @param mixed $isGroupPreSale
     */
    public function setIsGroupPreSale($isGroupPreSale)
    {
        $this->isGroupPreSale = $isGroupPreSale;
        $this->apiParas['is_group_pre_sale'] = $isGroupPreSale;
    }

    /**
     * 是否预售,true-预售商品，false-非预售商品
     * @param mixed $isPreSale
     */
    public function setIsPreSale($isPreSale)
    {
        $this->isPreSale = $isPreSale;
        $this->apiParas['is_pre_sale'] = $isPreSale;
    }

    /**
     * 是否7天无理由退换货，true-支持，false-不支持
     * @param mixed $isRefundable
     */
    public function setIsRefundable($isRefundable)
    {
        $this->isRefundable = $isRefundable;
        $this->apiParas['is_refundable'] = $isRefundable;
    }

    /**
     * 是否sku预售，1：是，0：否
     * @param mixed $isSkuPreSale
     */
    public function setIsSkuPreSale($isSkuPreSale)
    {
        $this->isSkuPreSale = $isSkuPreSale;
        $this->apiParas['is_sku_pre_sale'] = $isSkuPreSale;
    }

    /**
     * 缺重包退
     * @param mixed $lackOfWeightClaim
     */
    public function setLackOfWeightClaim($lackOfWeightClaim)
    {
        $this->lackOfWeightClaim = $lackOfWeightClaim;
        $this->apiParas['lack_of_weight_claim'] = $lackOfWeightClaim;
    }

    /**
     * 买家自提模版id
     * @param mixed $maiJiaZiTi
     */
    public function setMaiJiaZiTi($maiJiaZiTi)
    {
        $this->maiJiaZiTi = $maiJiaZiTi;
        $this->apiParas['mai_jia_zi_ti'] = $maiJiaZiTi;
    }

    /**
     * 是否提交草稿，默认提交 0:提交，1：保存
     * @param mixed $operateType
     */
    public function setOperateType($operateType)
    {
        $this->operateType = $operateType;
        $this->apiParas['operate_type'] = $operateType;
    }

    /**
     * 单次限量
     * @param mixed $orderLimit
     */
    public function setOrderLimit($orderLimit)
    {
        $this->orderLimit = $orderLimit;
        $this->apiParas['order_limit'] = $orderLimit;
    }

    /**
     * 原产地id，是指海淘商品的生产地址，仅在goods type=3/4的时候必填，可以通过pdd.goods.country.get获取
     * @param mixed $originCountryId
     */
    public function setOriginCountryId($originCountryId)
    {
        $this->originCountryId = $originCountryId;
        $this->apiParas['origin_country_id'] = $originCountryId;
    }

    /**
     * 商品goods外部编码，同其他接口中的outer_goods_id 、out_goods_id、out_goods_sn、outer_goods_sn 都为商家编码（goods维度）。
     * @param mixed $outGoodsId
     */
    public function setOutGoodsId($outGoodsId)
    {
        $this->outGoodsId = $outGoodsId;
        $this->apiParas['out_goods_id'] = $outGoodsId;
    }

    /**
     * 第三方商品Id
     * @param mixed $outSourceGoodsId
     */
    public function setOutSourceGoodsId($outSourceGoodsId)
    {
        $this->outSourceGoodsId = $outSourceGoodsId;
        $this->apiParas['out_source_goods_id'] = $outSourceGoodsId;
    }

    /**
     * 第三方商品来源
     * @param mixed $outSourceType
     */
    public function setOutSourceType($outSourceType)
    {
        $this->outSourceType = $outSourceType;
        $this->apiParas['out_source_type'] = $outSourceType;
    }

    /**
     * 海外商品信息
     * @param mixed $overseaGoods
     */
    public function setOverseaGoods($overseaGoods)
    {
        $this->overseaGoods = $overseaGoods;
        $this->apiParas['oversea_goods'] = $overseaGoods;
    }

    /**
     * 海外类型
     * @param mixed $overseaType
     */
    public function setOverseaType($overseaType)
    {
        $this->overseaType = $overseaType;
        $this->apiParas['oversea_type'] = $overseaType;
    }

    /**
     * 预售时间，is_pre_sale为true时必传，UNIX时间戳，只能为某一天的23:59:59
     * @param mixed $preSaleTime
     */
    public function setPreSaleTime($preSaleTime)
    {
        $this->preSaleTime = $preSaleTime;
        $this->apiParas['pre_sale_time'] = $preSaleTime;
    }

    /**
     * 0：不支持全国联保；1：支持全国联保
     * @param mixed $quanGuoLianBao
     */
    public function setQuanGuoLianBao($quanGuoLianBao)
    {
        $this->quanGuoLianBao = $quanGuoLianBao;
        $this->apiParas['quan_guo_lian_bao'] = $quanGuoLianBao;
    }

    /**
     * 是否二手商品，true -二手商品 ，false-全新商品
     * @param mixed $secondHand
     */
    public function setSecondHand($secondHand)
    {
        $this->secondHand = $secondHand;
        $this->apiParas['second_hand'] = $secondHand;
    }

    /**
     * 上门安装模版id
     * @param mixed $shangMenAnZhuang
     */
    public function setShangMenAnZhuang($shangMenAnZhuang)
    {
        $this->shangMenAnZhuang = $shangMenAnZhuang;
        $this->apiParas['shang_men_an_zhuang'] = $shangMenAnZhuang;
    }

    /**
     * 承诺发货时间（ 秒），48小时或24小时，is_pre_sale为true时不必传
     * @param mixed $shipmentLimitSecond
     */
    public function setShipmentLimitSecond($shipmentLimitSecond)
    {
        $this->shipmentLimitSecond = $shipmentLimitSecond;
        $this->apiParas['shipment_limit_second'] = $shipmentLimitSecond;
    }

    /**
     * 尺码表id
     * @param mixed $sizeSpecId
     */
    public function setSizeSpecId($sizeSpecId)
    {
        $this->sizeSpecId = $sizeSpecId;
        $this->apiParas['size_spec_id'] = $sizeSpecId;
    }

    /**
     * sku对象列表,实例：[{ "is_onsale": 1, "limit_quantity": 999, "price": "2200", "weight": 1000, "multi_price": "1900", "thumb_url": "http://t06img.yangkeduo.com/images/2018-04-15/ced035033b5d40b589140af882621c03.jpg", "out_sku_sn": "L", "quantity": 100, "spec_id_list": "[25]", "oversea_sku": { "measurement_code": "计量单位编码", "taxation": "税费", "specifications": "规格" } }]
     * @param mixed $skuList
     */
    public function setSkuList($skuList)
    {
        $this->skuList = $skuList;
        $this->apiParas['sku_list'] = $skuList;
    }

    /**
     * 库存方式（0：普通型，1：日历型）
     * @param mixed $skuType
     */
    public function setSkuType($skuType)
    {
        $this->skuType = $skuType;
        $this->apiParas['sku_type'] = $skuType;
    }

    /**
     * 送货入户并安装模版id
     * @param mixed $songHuoAnZhuang
     */
    public function setSongHuoAnZhuang($songHuoAnZhuang)
    {
        $this->songHuoAnZhuang = $songHuoAnZhuang;
        $this->apiParas['song_huo_an_zhuang'] = $songHuoAnZhuang;
    }

    /**
     * 送货入户模版id
     * @param mixed $songHuoRuHu
     */
    public function setSongHuoRuHu($songHuoRuHu)
    {
        $this->songHuoRuHu = $songHuoRuHu;
        $this->apiParas['song_huo_ru_hu'] = $songHuoRuHu;
    }

    /**
     * 商品短标题（仅在部分活动中生效），字数限制为4-20字
     * @param mixed $tinyName
     */
    public function setTinyName($tinyName)
    {
        $this->tinyName = $tinyName;
        $this->apiParas['tiny_name'] = $tinyName;
    }

    /**
     * 保税仓，只在goods_type为直供商品时有效（现阶段暂不支持）
     * @param mixed $warehouse
     */
    public function setWarehouse($warehouse)
    {
        $this->warehouse = $warehouse;
        $this->apiParas['warehouse'] = $warehouse;
    }

    /**
     * 水果类目温馨提示，只在水果类目商品才生效， 字数限制：商品描述goods_desc+温馨提示总计不超过500字。
     * @param mixed $warmTips
     */
    public function setWarmTips($warmTips)
    {
        $this->warmTips = $warmTips;
        $this->apiParas['warmTips'] = $warmTips;
    }

    /**
     * 只换不修的天数，目前只支持0和365
     * @param mixed $zhiHuanBuXiu
     */
    public function setZhiHuanBuXiu($zhiHuanBuXiu)
    {
        $this->zhiHuanBuXiu = $zhiHuanBuXiu;
        $this->apiParas['zhi_huan_bu_xiu'] = $zhiHuanBuXiu;
    }

    /**
     * 草稿id
     * @param mixed $goodsCommitId
     */
    public function setGoodsCommitId($goodsCommitId)
    {
        $this->goodsCommitId = $goodsCommitId;
        $this->apiParas['goods_commit_id'] = $goodsCommitId;
    }

    /**
     * @return mixed
     */
    public function getGoodsId()
    {
        return $this->goodsId;
    }

    /**
     * @return mixed
     */
    public function getOutGoodsId()
    {
        return $this->outGoodsId;
    }

    /**
     * @return mixed
     */
    public function getCatId()
    {
        return $this->catId;
    }

    /**
     * @return array
     */
    public function getApiParas()
    {
        return $this->apiParas;
    }

    public function getApiMethodName()
    {
        return "pdd.goods.edit.goods.commit";
    }

    /**
     * 验证基础规则
     */
    public function check()
    {
        RequestCheckUtil::checkNotNull($this->carouselGallery,"carousel_gallery");
        RequestCheckUtil::checkNotNull($this->catId,"cat_id");
        RequestCheckUtil::checkNotNull($this->costTemplateId,"cost_template_id");
        RequestCheckUtil::checkNotNull($this->countryId,"country_id");
        RequestCheckUtil::checkNotNull($this->detailGallery,"detail_gallery");
        RequestCheckUtil::checkNotNull($this->goodsDesc,"goods_desc");
        RequestCheckUtil::checkBetween($this->goodsDesc,2,50,"goods_desc");
//        RequestCheckUtil::checkNotNull($this->goodsId,"goods_id");
        RequestCheckUtil::checkNotNull($this->goodsName,"goods_name");
        RequestCheckUtil::checkNotNull($this->goodsType,"goods_type");
        RequestCheckUtil::checkInValue(
            $this->goodsType,
            [
                0,1,2,3,4,5,6,7,8,9,15,19
            ],
            "goods_type"
        );
        // 特定值
        $boolIn = ["true","false"];
        RequestCheckUtil::checkInValue($this->isFolt,$boolIn,"is_folt");
        RequestCheckUtil::checkInValue($this->isPreSale,$boolIn,"is_pre_sale");
        RequestCheckUtil::checkInValue($this->isRefundable,$boolIn,"is_refundable");
        RequestCheckUtil::checkNotNull($this->marketPrice,"market_price");
        RequestCheckUtil::checkInValue($this->secondHand,$boolIn,"second_hand");
        RequestCheckUtil::checkNotNull($this->shipmentLimitSecond,"shipment_limit_second");
        RequestCheckUtil::checkNotNull($this->skuList,"sku_list");
        
    }

    public function putOuterTextParam($key, $value)
    {
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }

}