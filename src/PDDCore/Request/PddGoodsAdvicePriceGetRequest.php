<?php
/**
 * pdd.goods.advice.price.get商品建议价格获取接口
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/9
 * +-----------------------------
 * Time: 17:44
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Request;


use PDDCore\Contract\GoodsInterface;
use PDDCore\RequestCheckUtil;

class PddGoodsAdvicePriceGetRequest implements GoodsInterface
{
    private $request;

    private $apiParas=[];

    /**
     * 获取商品建议价格请求参数
     * @param mixed $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
        $this->apiParas['request'] = $request;
    }

    /**
     * @return array
     */
    public function getApiParas()
    {
        return $this->apiParas;
    }

    /**
     * @return mixed
     */
    public function getRequest()
    {
        return $this->request;
    }

    public function getApiMethodName()
    {
        return "pdd.goods.advice.price.get";
    }

    public function check()
    {
        RequestCheckUtil::checkNotNull($this->request,"request");
    }

    public function putOuterTextParam($key, $value)
    {
        // TODO: Implement putOuterTextParam() method.
        $this->apiParas[$key] = $value;
        $this->$key = $value;
    }


}