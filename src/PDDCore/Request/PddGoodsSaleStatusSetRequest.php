<?php
/**
 *
 * +-----------------------------
 * Class name: PddGoodsSaleStatusSetRequest
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2023/5/9
 * +-----------------------------
 * Time: 10:12
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * by PHPstorm
 * +-----------------------------
 */

namespace PDDCore\Request;


use PDDCore\Contract\GoodsInterface;
use PDDCore\RequestCheckUtil;

class PddGoodsSaleStatusSetRequest implements GoodsInterface
{
    private $apiParas = [];

    private $goodsId;

    private $isOnsale;

    /**
     * @param mixed $isOnsale
     */
    public function setIsOnsale($isOnsale): void
    {
        $this->isOnsale = $isOnsale;
        $this->apiParas['is_onsale'] = $isOnsale;
    }

    /**
     * @param mixed $goodsId
     */
    public function setGoodsId($goodsId): void
    {
        $this->goodsId = $goodsId;
        $this->apiParas['goods_id'] = $goodsId;
    }

    public function check()
    {
        // TODO: Implement check() method.
        RequestCheckUtil::checkNotNull($this->goodsId, 'goods_id');
        RequestCheckUtil::checkInValue($this->isOnsale, [0, 1], "is_onsale");
    }
    public function getApiMethodName()
    {
        // TODO: Implement getApiMethodName() method.
        return "pdd.goods.sale.status.set";
    }
    public function getApiParas()
    {
        // TODO: Implement getApiParas() method.
        return $this->apiParas;
    }
    public function putOuterTextParam($key, $value)
    {
        // TODO: Implement putOuterTextParam() method.
        $this->$key = $value;
        $this->apiParas[$key] = $value;
    }
}
