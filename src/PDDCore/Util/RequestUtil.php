<?php
/**
 * pdd.goods.advice.price.get商品建议价格[request]
 * 文档：https://open.pinduoduo.com/application/document/api?id=pdd.goods.advice.price.get
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/9
 * +-----------------------------
 * Time: 17:51
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Util;


class RequestUtil
{
    /**
     * 页码，默认1
     * @var
     */
    public $page;

    /**
     * 每页数量，默认100，最大100
     * @var
     */
    public $page_size;

}