<?php
/**
 * pdd.goods.information.update商品编辑|添加，sku对象列表[sku_list]，海外扩张SKU信息[oversea_sku]
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/11
 * +-----------------------------
 * Time: 17:16
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Util;


class SkuListOverseaSkuUtil
{
    /**
     * 计量单位编码，从接口pdd.gooods.sku.measurement.list获取code
     * @var
     */
    public $measurement_code;

    /**
     * 规格
     * @var
     */
    public $specifications;

    /**
     * 税费
     * @var
     */
    public $taxation;
}