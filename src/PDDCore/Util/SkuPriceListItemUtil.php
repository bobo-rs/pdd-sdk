<?php
/**
 * pdd.goods.sku.price.update修改商品sku价格[sku_price_list]
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/11
 * +-----------------------------
 * Time: 14:33
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Util;


class SkuPriceListItemUtil
{
    /**
     * 拼团购买价格（单位分）
     * @var
     */
    public $group_price;

    /**
     * sku上架状态，0-已下架，1-上架中
     * @var
     */
    public $is_onsale;

    /**
     * 单独购买价格（单位分）
     * @var
     */
    public $single_price;

    /**
     * sku标识
     * @var
     */
    public $sku_id;
}