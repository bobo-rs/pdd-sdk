<?php
/**
 * pdd.goods.spu.search标品搜索接口|pdd.goods.spu.get标品详情接口[key_prop]属性
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/11
 * +-----------------------------
 * Time: 11:24
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Util;


class KeyPropItemUtil
{
    /**
     * 关键属性的引用属性ID，需要从pdd.cat.rule.get中获取。
     * @var
     */
    public $ref_pid;

    /**
     * 属性值单位
     * @var
     */
    public $value_unit;

    /**
     * 关键属性值，需要从pdd.goods.cat.template.get中获取。当要根据关键属性匹配时，和vid必须入参其一。
     * @var
     */
    public $value;

    /**
     * 关键属性值ID，需要从pdd.goods.cat.template.get中获取规则。当要根据关键属性匹配时，和value必须入参其一。
     * @var
     */
    public $vid;
}