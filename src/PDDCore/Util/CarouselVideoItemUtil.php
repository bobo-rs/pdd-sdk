<?php
/**
 * pdd.goods.information.update商品编辑|添加，属性[carousel_video]
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/11
 * +-----------------------------
 * Time: 16:36
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Util;


class CarouselVideoItemUtil
{
    /**
     * 商品视频id
     * @var
     */
    public $file_id;

    /**
     * 商品视频url
     * @var
     */
    public $video_url;
}