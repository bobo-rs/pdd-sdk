<?php
/**
 * pdd.goods.information.update商品编辑|添加，卡券类商品属性[carousel_video]
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/11
 * +-----------------------------
 * Time: 16:39
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Util;


class ElecGoodsAttributesUtil
{
    /**
     * 开始时间（timeType=1时必填表示核销的开始时间）（精确到毫秒）
     * @var
     */
    public $begin_time;

    /**
     * 天数内有效（timeType=3必填，表示发货后几天内核销）
     * @var
     */
    public $days_time;

    /**
     * 截止时间（timeType=1,2时必填，表示发货后核销的截止时间）（精确到毫秒）
     * @var
     */
    public $end_time;

    /**
     * 卡券核销类型（1：起始时间内有效，2：发货后后至截止时间内有效，3：发货后多少天内有效）
     * @var
     */
    public $time_type;
}