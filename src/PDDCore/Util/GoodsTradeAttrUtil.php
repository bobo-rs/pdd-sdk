<?php
/**
 * pdd.goods.information.update商品编辑|添加，日历商品交易相关信息[goods_trade_attr]
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/11
 * +-----------------------------
 * Time: 16:51
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Util;


class GoodsTradeAttrUtil
{
    /**
     * 提前预定天数，默认为0表示当天可预定
     * @var
     */
    public $advances_days;

    /**
     * 卡券有效期，日历日期后多少天可用。默认值为0表示仅限日历日当天使用
     * @var
     */
    public $life_span;

    /**
     * 预订须知
     * @var
     */
    public $booking_notes;

}