<?php
/**
 * pdd.goods.information.update商品编辑|添加，sku对象列表[sku_list]，sku属性[sku_properties]
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/11
 * +-----------------------------
 * Time: 17:23
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Util;


class SkuListSkuPropertiesItemUtil
{
    /**
     * 属性单位
     * @var
     */
    public $punit;

    /**
     * 属性id
     * @var
     */
    public $ref_pid;

    /**
     * 属性值
     * @var
     */
    public $value;

    /**
     * 属性值id
     * @var
     */
    public $vid;
}