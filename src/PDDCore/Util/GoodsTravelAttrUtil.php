<?php
/**
 * pdd.goods.information.update商品编辑|添加，商品出行信息[goods_travel_attr]
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/11
 * +-----------------------------
 * Time: 17:01
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Util;


class GoodsTravelAttrUtil
{
    /**
     * 出行人是否必填（默认是）
     * @var
     */
    public $need_tourist;

    /**
     * 日历商品类型1:旅行类,2:住宿类,3:票务类
     * @var
     */
    public $type;
}