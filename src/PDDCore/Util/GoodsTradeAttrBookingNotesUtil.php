<?php
/**
 * pdd.goods.information.update商品编辑|添加，
 * 日历商品交易相关信息[goods_trade_attr],预订须知[booking_notes]
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/11
 * +-----------------------------
 * Time: 16:59
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Util;


class GoodsTradeAttrBookingNotesUtil
{
    /**
     * 预定须知图片地址
     * @var
     */
    public $url;
}