<?php
/**
 * pdd.goods.information.update商品编辑|添加，海外商品税率信息[oversea_goods]
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/11
 * +-----------------------------
 * Time: 17:03
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Util;


class OverseaGoodsUtil
{
    /**
     * 保税仓唯一标识
     * @var
     */
    public $bonded_warehouse_key;

    /**
     * 消费税率
     * @var
     */
    public $consumption_tax_rate;

    /**
     * 清关服务商
     * @var
     */
    public $customs_broker;

    /**
     * 海关编号
     * @var
     */
    public $hs_code;

    /**
     * 增值税率
     * @var
     */
    public $value_added_tax_rate;
}