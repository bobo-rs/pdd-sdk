<?php
/**
 * pdd.goods.information.update商品编辑|添加，商品属性列表[goods_properties]
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/11
 * +-----------------------------
 * Time: 16:43
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Util;


class GoodsPropertiesItemUtil
{
    /**
     * 组id，非销售属性不用传
     * @var
     */
    public $group_id;

    /**
     * 图片url，非销售属性不用传
     * @var
     */
    public $image_url;

    /**
     * 备注，非销售属性不用传
     * @var
     */
    public $note;

    /**
     * 父属性id，非销售属性不用传
     * @var
     */
    public $parent_spec_id;

    /**
     * 引用属性id
     * @var
     */
    public $ref_pid;

    /**
     * 属性id，非销售属性不用传
     * @var
     */
    public $spec_id;

    /**
     * 模板属性id
     * @var
     */
    public $template_pid;

    /**
     * 属性值
     * @var
     */
    public $value;

    /**
     * 属性单位
     * @var
     */
    public $value_unit;

    /**
     * 属性值id
     * @var
     */
    public $vid;
}