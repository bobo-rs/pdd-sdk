<?php
/**
 * pdd.goods.information.update商品编辑|添加，sku对象列表[sku_list]
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/11
 * +-----------------------------
 * Time: 17:07
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

namespace PDDCore\Util;


class SkuListItemUtil
{
    /**
     * sku上架状态，0-已下架，1-上架中
     * @var
     */
    public $is_onsale;

    /**
     * sku送装参数：长度
     * @var
     */
    public $length;

    /**
     * sku购买限制，只入参999
     * @var
     */
    public $limit_quantity;

    /**
     * 商品团购价格
     * @var
     */
    public $multi_price;

    /**
     * 商品sku外部编码，同其他接口中的outer_id 、out_id、out_sku_sn、outer_sku_sn、out_sku_id、outer_sku_id 都为商家编码（sku维度）。
     * @var
     */
    public $out_sku_sn;

    /**
     * 第三方sku Id
     * @var
     */
    public $out_source_sku_id;

    /**
     * 海外扩张SKU信息
     * @var
     */
    public $oversea_sku;

    /**
     * 商品单买价格
     * @var
     */
    public $price;

    /**
     * 商品sku库存增减数量，可入参值为正负0整数。后续库存update建议只使用stocks.update接口进行调用
     * @var
     */
    public $quantity;

    /**
     * sku编码，如果传值，则在原sku基础上进行编辑，如果传空，则新增sku。
     * @var
     */
    public $sku_id;

    /**
     * 商品规格列表，根据pdd.goods.spec.id.get生成的规格属性id，例如：颜色规格下商家新增白色和黑色，大小规格下商家新增L和XL，则由4种spec组合，入参一种组合即可，在skulist中需要有4个spec组合的sku
     * @var
     */
    public $spec_id_list;

    /**
     * sku缩略图
     * @var
     */
    public $thumb_url;

    /**
     * 重量，单位为g
     * @var
     */
    public $weight;

    /**
     * sku预售时间戳，单位秒；不更新传null，取消传0，更新传实际值
     * @var
     */
    public $sku_pre_sale_time;

    /**
     * sku属性
     * @var
     */
    public $sku_properties;

    /**
     * @param mixed $oversea_sku
     */
    public function setOverseaSku($oversea_sku)
    {
        $this->oversea_sku = $oversea_sku;
    }

    /**
     * @param mixed $sku_properties
     */
    public function setSkuProperties($sku_properties)
    {
        $this->sku_properties = $sku_properties;
    }
}