<?php
/**
 * 数组列表转JSON
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/14
 * +-----------------------------
 * Time: 17:18
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * by 160大药房
 * +-----------------------------
 */

namespace PDDCore;


class ArrayList
{

    /**
     * 转换添加到对象属性
     * @param array $data
     * @return string
     */
    public function add(array $data)
    {
        return json_encode(
            $data,JSON_UNESCAPED_UNICODE
        );
    }
}