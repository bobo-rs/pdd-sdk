<?php
/**
 * 调试图片上传
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/12
 * +-----------------------------
 * Time: 10:26
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

require_once "loader.php";

use PDDCore\Request;

$gatewayUrl = "https://gw-upload.pinduoduo.com/api/upload";

$fileUrl = $_POST['file'];
if (!$fileUrl){
    exit(json_encode([
        "code"=>1001,
        "msg"=>"请上传文件~"
    ],JSON_UNESCAPED_UNICODE));
}

//$fileUrl = "https://t00img.yangkeduo.com/garner-api-open/59ad8dfcbc424745b1a88aa0ffae8a5e.jpg";
$client = new PDDCore\TopClient(APPKEY,SECRET);
$client->setGatewayUrl($gatewayUrl);
// 上传文件
$requset = new Request\PddGoodsFilespaceImageUploadRequest();
$requset->setFile($fileUrl); // 注：此字段不参与签名

// 执行请求
$res = $client->execute($requset,ACCESS_TOKEN,'file');
exit(json_encode($res,JSON_UNESCAPED_UNICODE));