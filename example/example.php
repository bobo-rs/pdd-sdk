<?php
/**
 * 调试入口
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/9
 * +-----------------------------
 * Time: 15:49
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */

require_once "loader.php";

use PDDCore\Request;

$data = [];
if ($_GET){
    $data = $_GET;
}elseif($_POST){
    $data = $_POST;
}
if (!$data){
    exit(json_encode([
        "code"=>1001,
        "msg"=>"参数不能为空~",
        "data"=>[]
    ],JSON_UNESCAPED_UNICODE));
}
try{
    $check = \PDDCore\RequestCheckUtil::checkInValue(1,[1,3],"fffff");
}catch (\Exception $e){
    exit(json_encode([
        "code"=>$e->getCode(),
        "error_msg"=>$e->getMessage(),
        "desc"=>$e->getTraceAsString()
    ],JSON_UNESCAPED_UNICODE));
}

$client = new PDDCore\TopClient(APPKEY,SECRET);
$client->setGatewayUrl(GATEWAY_URL);

// 商品列表
$req = new Request\PddGoodsListGetRequest();
$req->setPageSize($data['pageSize']);
$req->setPage($data['page']);

// 商品详情
//$req = new Request\PddGoodsInformationGetRequest();
//$req->setGoodsId($data['goodsId']);

$res = $client->execute($req,ACCESS_TOKEN);
exit(json_encode(
    $res,JSON_UNESCAPED_UNICODE
));


