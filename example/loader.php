<?php
/**
 * 自动加载配置
 * +-----------------------------
 * User: BOBO
 * +-----------------------------
 * Date: 2021/10/12
 * +-----------------------------
 * Time: 10:42
 * +-----------------------------
 * Created by PhpStorm.
 * +-----------------------------
 * Copyright (c) 2020~2031
 * +-----------------------------
 */
header("Content-Type: text/html; charset=utf-8");
date_default_timezone_set('Asia/Shanghai');
// 加载类文件
define("ROOT_PATH",dirname(__DIR__));
require_once ROOT_PATH."/autoload.php";

// 配置
const APPKEY = "xxxxxxxxxxxxxxxxxxxxx";
const SECRET = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
const ACCESS_TOKEN = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
const GATEWAY_URL = "https://gw-api.pinduoduo.com/api/router";